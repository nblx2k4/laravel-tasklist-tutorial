<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Laravel Quickstart - Basic</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <!- CSS and JS-->
        <link rel="stylesheet" href="/template/css/bootstrap.min.css"/>
        <link rel="stylesheet" href="/template/css/bootstrap-theme.min.css"/>
        
        <script type="text/javascript" src="/template/js/bootstrap.min.js"></script>
    </head>
    <body>
        <div class="container">
            <nav class="navbar navbar-default">
            
            </nav>
        </div>
        
        @yield('content')
    </body>
</html>