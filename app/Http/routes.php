<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    $tasks = Task::orderBy('created_at', 'asc')->get();
    
    return view('tasks', [
        'tasks' => $tasks
    ]);
});

Route::post('/task', function(\Illuminate\Http\Request $request){

    $validator = Validator::make($request->all(), [
        'name' => 'required|max:255'
    ]);
    
    if($validator->fails())
    {
        return redirect()
            ->withInput()
            ->withErrors($validator);
    }
    
    // Create the task
    $task = new \App\Task();
    $task->name = $request->name;
    $task->save();
    
    return redirect('/');
});

Route::delete('/task/{id}', function($id){
	
});